# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

ZSH_THEME="norm"

TERM="tmux-256color"
plugins=(git wd sudo rand-quote)

source $ZSH/oh-my-zsh.sh

alias la='ls -a'


# User configuration

export PATH="$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
echo UPDATESTARTUPTTY | gpg-connect-agent &> /dev/null
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"

bindkey '\e[A' history-search-backward
bindkey '\e[B' history-search-forward

alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'

alias :q='exit'
alias kthxbai="shutdown -h now"

# Generate ctags excluding git and build
alias cgen='ctags -R --exclude=.git --exclude=build --exclude=target'
alias gr='~/.config/awesome/scripts/utils/grep.sh'

export PATH="/usr/bin/core_perl:$PATH"
export PATH="/usr/lib/ccache/bin:$PATH"
export PATH="/usr/lib/ccache:$PATH"
export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:$HOME/go/bin"
export PATH="$PATH:$HOME/.local/bin"
export EDITOR="nvim"
export GOPATH="$HOME/go"

# ccache?
export CCACHE_MAXSIZE=10G
export CCACHE_CPP2=true
export CCACHE_HARDLINK=true
export CCACHE_SLOPPINESS=file_macro,time_macros,include_file_mtime,include_file_ctime,file_stat_matches
#alias clang='ccache /usr/bin/clang-5.0 "$@"'


alias cclip='xclip -selection clipboard'

alias worm="~/.config/awesome/scripts/frosting/ternimal height=132 width=200 length=100 gradient=0:\#03001e,0.25:\#7303c0,0.5:\#ec38bc,1:\#F89EDF"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"
#[[ -s "/usr/loca/rvm/scripts/rvm" ]] && source "/usr/local/rvm/scripts/rvm"

#function git-current-branch {
#    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1) /'
#}
#export PS1="[ruby-\$(~/.rvm/bin/rvm-prompt v p g)]\$(git-current-branch)\n$PS1"

alias gs="git status"
alias gp="git pull"
alias gc="git commit -v"
alias gn="git nuke"
alias gpu="git push"
alias gl="git lg"

alias tf="terraform"

export VIM_APP_DIR=/usr
alias vim="nvim"

# Rust MUSL
alias rust-musl-builder='docker run --rm -it -v "$(pwd)":/home/rust/src ekidd/rust-musl-builder:69'
export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

# Rust RR
alias rrdb="rr replay -d rust-gdb"

# CPU stats
cpu() {
    while true; do vmstat -s | awk  ' $0 ~ /total memory/ {total=$1 } $0 ~/free memory/ {free=$1} $0 ~/buffer memory/ {buffer=$1} $0 ~/cache/ {cache=$1} END {rounded = sprintf("%.1f", (total-free-buffer-cache)/total*100); print rounded}' && top -n1 | grep "us," | cut -c26-29 | xargs && sleep 1; done | ttyplot -s 100 -t "mem/cpu usage" -u "%" -2
}

# git-find-file
git-find-file() {
    for branch in $(git rev-list --all)
    do
      if (git ls-tree -r --name-only $branch | grep --quiet "$1")
      then
         echo $branch
      fi
    done
}

# Sprunge
sprunge() {
    if [[ $1 ]]; then
        curl -F 'sprunge=<-' "http://sprunge.us" <"$1"
    else
        curl -F 'sprunge=<-' "http://sprunge.us"
    fi
}

# Examples:
#     ix hello.txt              # paste file (name/ext will be set).
#     echo Hello world. | ix    # read from STDIN (won't set name/ext).
#     ix -n 1 self_destruct.txt # paste will be deleted after one read.
#     ix -i ID hello.txt        # replace ID, if you have permission.
#     ix -d ID

ix() {
    local opts
    local OPTIND
    [ -f "$HOME/.netrc" ] && opts='-n'
    while getopts ":hd:i:n:" x; do
        case $x in
            h) echo "ix [-d ID] [-i ID] [-n N] [opts]"; return;;
            d) $echo curl $opts -X DELETE ix.io/$OPTARG; return;;
            i) opts="$opts -X PUT"; local id="$OPTARG";;
            n) opts="$opts -F read:1=$OPTARG";;
        esac
    done
    shift $(($OPTIND - 1))
    [ -t 0 ] && {
        local filename="$1"
    shift
    [ "$filename" ] && {
        local cmd="curl $opts -F f:1=@$filename $* ix.io/$id"
    bash -c $cmd
    return
}
echo "^C to cancel, ^D to send."
    }
    curl $opts -F f:1='<-' $* ix.io/$id
}

pass() {
    local len="$1"
    < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32};echo;
}

# Add Ruby gems to PATH
if which ruby >/dev/null && which gem >/dev/null; then
    PATH="$(ruby -r rubygems -e 'puts Gem.user_dir')/bin:$PATH"
fi

PATH="/home/lain/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/lain/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/lain/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/lain/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/lain/perl5"; export PERL_MM_OPT;

if [ "$TMUX" = "" ]; then tmux; fi
