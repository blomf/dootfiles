call plug#begin('~/.vim/plugged')
    Plug 'mattn/calendar-vim'
    Plug 'sheerun/vim-polyglot'
    Plug 'vimwiki/vimwiki'
    Plug 'skywind3000/vim-preview'
    Plug 'kshenoy/vim-signature'
    Plug 'liuchengxu/space-vim-dark'
    Plug 'easymotion/vim-easymotion'
    Plug 'jeetsukumaran/vim-buffergator'
    Plug 'tpope/vim-fugitive'
    Plug 'terryma/vim-multiple-cursors'
    Plug 'scrooloose/nerdcommenter'
    Plug 'scrooloose/nerdtree'
"    Plug 'ervandew/supertab'
    Plug 'vim-syntastic/syntastic'
    Plug 'OmniSharp/omnisharp-vim'
"    Plug 'w0rp/ale'
    Plug 'neoclide/coc.nvim', {'tag': '*', 'do': { -> coc#util#install()}}
    Plug 'bronson/vim-trailing-whitespace'
    Plug 'ronakg/quickr-cscope.vim'
    Plug 'rust-lang/rust.vim'
    Plug 'majutsushi/tagbar'
"    Plug 'racer-rust/vim-racer'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'godlygeek/tabular'
    Plug 'plasticboy/vim-markdown'
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
    Plug 'powerman/vim-plugin-AnsiEsc'
    Plug 'PProvost/vim-ps1'
    Plug 'hashivim/vim-terraform'
    Plug 'mattn/webapi-vim'
    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'vim-scripts/restore_view.vim'
    Plug 'tpope/vim-eunuch'
    Plug 'tpope/vim-unimpaired'
    Plug 'tpope/vim-obsession'
"    Plug 'tpope/vim-endwise'
    Plug 'wannesm/wmgraphviz.vim'
    Plug 'liuchengxu/graphviz.vim'
"    if has('nvim')
"        Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"    else
"        Plug 'Shougo/deoplete.nvim'
"        Plug 'roxma/nvim-yarp'
"        Plug 'roxma/vim-hug-neovim-rpc'
"    endif
"    Plug 'sebastianmarkow/deoplete-rust'

call plug#end()
" air-line
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''


set t_Co=256
syntax on

set autoindent
set background=dark
set clipboard=unnamedplus
set colorcolumn=80
set complete+=kspell
"set cursorline
"set cursorcolumn
set expandtab
set fdm=syntax
set fdls=99
set formatoptions-=t
set hidden
set history=1000                " Remember more commands
set laststatus=2 " Always display the statusline in all windows
set list listchars=tab:→\ ,trail:·
set mouse=a
set nocompatible
set nowrap
set number
set regexpengine=1 " Holy shit dude, fuck lag
set shiftwidth=4
set shortmess+=c
set showtabline=2 " Always display the tabline, even if there is only one tab
set smarttab
set smartindent
set so=10
set spelllang=en_us
set tabstop=4
set tags=./tags;
set wrap!
set updatetime=0


if has('persistent_undo')
    set undofile                " Persistent undo
    set undodir=~/.vim/undo     " Location to store undo history
    set undolevels=1000         " Max number of changes
    set undoreload=10000        " Max lines to save for undo on a buffer reload
endif

let &t_ZH="\e[3m"
let &t_ZR="\e[23m"

filetype plugin on
hi Comment cterm=bold
color space-vim-dark
highlight ColorColumn ctermbg=235
hi Normal ctermbg=none
highlight Comment cterm=italic
hi! link markdownItalic Italic
hi! link markdownBold Bold

highlight htmlBold gui=bold cterm=bold
highlight htmlItalic gui=italic cterm=italic


"""""""""""""""""""""""""""""""""""""
""""""""""""vim taskwiki"""""""""""""
"""""""""""""""""""""""""""""""""""""
let pup_wiki = {}
let pup_wiki.path = '~/pup/brain/'
let pup_wiki.syntax = 'markdown'
let pup_wiki.ext = '.md'
let pup_wiki.nested_syntaxes = {'python': 'python', 'c':'c', 'c++':'cpp', 'rust':'rust'}
let pup_wiki.path_html = '~/pup/html/'
let pup_wiki.template_path = pup_wiki.path_html . '/template'
let pup_wiki.template_default = 'default'
let pup_wiki.template_ext = '.htm'
let pup_wiki.auto_toc = 1
let pup_wiki.auto_tags = 1
let pup_wiki.auto_diary_index = 1

"hi VimwikiHeader1 guifg=#FF0000
"hi VimwikiHeader2 guifg=#00FF00
"hi VimwikiHeader3 guifg=#0000FF
"hi VimwikiHeader4 guifg=#FF00FF
"hi VimwikiHeader5 guifg=#00FFFF
"hi VimwikiHeader6 guifg=#FFFF00

let g:vimwiki_ext2syntax = {'.md':'markdown'}
let g:vimwiki_global_ext = 0
let g:taskwiki_syntax = 'markdown'

let g:vimwiki_list = [pup_wiki]

imap <C-E> <Plug>VimwikiIncreaseLvlSingleItem <C-I>

""""""""""""""""""""""""""""""""""""

function! RestoreRegister()
    let @" = s:restore_reg
    if &clipboard == "unnamedplus"
        let @* = s:restore_reg
    endif
    return ''
endfunction

"""""""""
" Remaps"
"""""""""
" fuck clipboards
nnoremap  dd "_dd
nnoremap <nowait>  d  "_d

nmap <nowait> f <Plug>(easymotion-bd-w)

function! s:Repl()
    let s:restore_reg = @"
    return "p@=RestoreRegister()\<cr>"
endfunction
vnoremap <silent> <expr> p <sid>Repl()

" Space folds
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space")<CR>

" Move Ctrl-j/k
nnoremap <silent> <C-j> :m .+1<CR>==
nnoremap <silent> <C-k> :m .-2<CR>==
inoremap <silent> <C-j> <Esc>:m .+1<CR>==gi
inoremap <silent> <C-k> <Esc>:m .-2<CR>==gi
vnoremap <silent> <C-j> :m '>+1<CR>gv=gv
vnoremap <silent> <C-k> :m '<-2<CR>gv=gv

" Move to the next buffer
nmap <nowait> <leader>l :bnext<CR>

" Move to the previous buffer
nmap <nowait> <leader>h :bprevious<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <nowait> <leader>q :bp <BAR> bd! #<CR>

" Show all open buffers and their status
nmap <nowait> <leader>bl :ls<CR>

nnoremap <silent> <Leader>> :vertical resize +5<CR>
nnoremap <silent> <Leader>< :vertical resize -5<CR>


" center cursor
"nnoremap j jzz
"nnoremap k kzz

"""""""""
"Configs"
"""""""""
" vim-one configs
let g:one#handleSwapfileConflicts = 1
let g:one#autocloseOpenedBuffers = 1
set splitright

" vim-markdown
set conceallevel=2
let g:vim_markdown_toc_autofit = 1
let g:vim_markdown_follow_anchor = 1
let g:vim_markdown_no_extensions_in_markdown = 1

" graphviz
let g:WMGraphviz_output = "png"
let g:WMGraphviz_viewer = "firefox-developer-edition"

" Terraform configs
let g:terraform_align=1
let g:terraform_fold_sections=1
let g:terraform_fmt_on_save=1
let g:terraform_completion_keys = 1


" Rust Shit
"""""""""""
"let g:rustfmt_autosave = 1
"
let g:rust_clip_command = 'xclip -selection clipboard'
let g:rust_cargo_avoid_whole_workspace = 0

" Tagbar
let g:tagbar_type_rust = {
    \ 'ctagstype' : 'rust',
    \ 'kinds' : [
        \'T:types,type definitions',
        \'f:functions,function definitions',
        \'P:functions,function definitions',
        \'g:enum,enumeration names',
        \'s:structure names',
        \'m:modules,module names',
        \'n:modules,module names',
        \'c:consts,static constants',
        \'t:traits',
        \'i:impls,trait implementations',
    \]
    \}
let g:tagbar_autofocus = 0
nmap <leader><leader>rt :TagbarToggle<CR>
nmap <leader>rt :TagbarOpen j<CR>
":TagbarTogglePause<CR>

function! ToggleErrors()
    if empty(filter(tabpagebuflist(), 'getbufvar(v:val, "&buftype") is# "quickfix"'))
         " No location/quickfix list shown, open syntastic error location panel
         Errors
    else
        lclose
    endif
endfunction

nnoremap <silent> <C-e> :<C-u>call ToggleErrors()<CR>
nnoremap <Leader>t :RustTest! -- --nocapture<CR>



" Completeopt
"set splitbelow
"set completeopt=menu,menuone,noselect
"inoremap <silent><expr> <Tab>
"      \ pumvisible() ? "\<C-n>" : "\<TAB>"

" UltiSnips
" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"


"" CoC
" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <C-l> for select text for visual placeholder of snippet.
vmap <C-l> <Plug>(coc-snippets-select)

" Use <C-l> for jump to next placeholder
let g:coc_snippet_next = '<c-l>'

" Use <C-h> for jump to previous placeholder
let g:coc_snippet_prev = '<c-h>'

" Use <C-l> for both expand and jump (make expand higher priority.)
imap <C-l> <Plug>(coc-snippets-expand-jump)

let g:airline_section_error = '%{airline#util#wrap(airline#extensions#coc#get_error(),0)}'
let g:airline_section_warning = '%{airline#util#wrap(airline#extensions#coc#get_warning(),0)}'

" Use <cr> for confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
"inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` for navigate diagnostics
nmap <silent> <S-k> <Plug>(coc-diagnostic-prev)
nmap <silent> <S-j> <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use L for show documentation in preview window
nnoremap <silent> L :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

hi default CocHighlightText ctermbg=236

" Confirm completion
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : 
                                           \"\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)
nmap <leader>rf <Plug>(coc-refactor)

" Remap for format selected region
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use `:Format` for format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` for fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" NERDComment
let g:NERDCompactSexyComs=1
let g:NERDDefaultAlign = 'left'
let g:NERDTrimTrailingWhitespace = 1
nnoremap <silent> <Leader>cc /{<CR>v% :call NERDComment("n", "Toggle")<CR> :noh<CR>

" NERDTree
map <leader>n :NERDTreeFocus<CR>
let g:NERDTreeWinSize=20
"let g:NERDTreeQuitOnOpen=1

" EasyAlign maybe?
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" Buffergator
let g:buffergator_show_full_directory_path = 0
let g:buffergator_suppress_keymaps = 1
" Fixes buffergator lag
nmap <nowait> <leader>b :BuffergatorToggle<CR>

" Omnisharp
let g:OmniSharp_server_stdio = 1
let g:OmniSharp_highlight_types = 3
let g:OmniSharp_highlight_groups = {
\ 'csUserIdentifier': [
\   'constant name', 'enum member name', 'field name', 'identifier',
\   'local name', 'parameter name', 'property name', 'static symbol'],
\ 'csUserInterface': ['interface name'],
\ 'csUserMethod': ['extension method name', 'method name'],
\ 'csUserType': ['class name', 'enum name', 'namespace name', 'struct name']
\}

" Syntastic
let g:syntastic_cs_checkers = ['code_checker']
let g:syntastic_terraform_tffilter_plan = 1
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 2
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_loc_list_height = 5

" Disable syntastic for Rust code
let g:syntastic_mode_map = {
    \ "mode": "active",
    \ "passive_filetypes": ["rust", "rs"] }



"""""""""""
""" OLD """
"""""""""""

"" Ale
"let g:ale_sign_column_always = 1
"let g:ale_linters = {'rust': ['rls']}
"let g:ale_rust_rls_toolchain = nightly-2018-12-06
"let g:ale_fix_on_save = 1
"let g:ale_lint_on_enter = 1
"let g:ale_lint_on_insert_leave = 0
"let g:ale_rust_cargo_check_all_targets = 1
"let g:ale_rust_cargo_avoid_whole_workspace = 0
"let g:ale_rust_cargo_use_clippy = 1
"let g:ale_fixers = {
"    \   'rust': ['trim_whitespace', 'rustfmt'],
"    \   'javascript': ['prettier'],
"    \   'css': ['prettier'],
"    \    'markdown': ['trim_whitespace', 'prettier'],
"    \}
"let g:ale_lint_on_text_changed = 0
"let g:ale_completion_delay = 150
"let g:ale_javascript_prettier_options = '--single-quote --trailing-comma es5'
"
"nmap <silent> <S-k> <Plug>(ale_previous_wrap)
"nmap <silent> <S-j> <Plug>(ale_next_wrap)

"" This allows buffers to be hidden if you've modified a buffer.
"" This is almost a must if you wish to use buffers in this way.
"set hidden
"
"" To open a new empty buffer
"" This replaces :tabnew which I used to bind to this mapping
"nmap <nowait> <leader>T :enew<cr>
"
"" Move to the next buffer
"nmap <nowait> <leader>l :bnext<CR>
"
"" Move to the previous buffer
"nmap <nowait> <leader>h :bprevious<CR>
"
"" Close the current buffer and move to the previous one
"" This replicates the idea of closing a tab
"nmap <nowait> <leader>bq :bp <BAR> bd #<CR>
"
"" Show all open buffers and their status
"nmap <nowait> <leader>bl :ls<CR>

" Deoplete
"let g:deoplete#enable_at_startup = 1

"let g:racer_experimental_completer = 1
"let g:deoplete#sources#rust#racer_binary='/home/lain/.cargo/bin/racer'
"let g:racer_cmd='/home/lain/.cargo/bin/racer'

" YouCompleteMe
"let g:ycm_complete_in_comments=1
"let g:ycm_server_python_interpreter = '/usr/bin/python'
"let g:ycm_collect_identifiers_from_tags_files=1
""let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
"let g:ycm_confirm_extra_conf = 0
"set completeopt-=preview
"let g:ycm_add_preview_to_completeopt = 0
"let g:ycm_min_num_of_chars_for_completion=3
"let g:ycm_seed_identifiers_with_syntax=1

" make YCM compatible with UltiSnips (using supertab)
"let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
"let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
"let g:SuperTabDefaultCompletionType = '<C-n>'
"nnoremap <F11> :YcmForceCompileAndDiagnostics<CR>
"nnoremap <F12> :YcmDiags<CR>

"nnoremap <silent> <Leader>yd :YcmCompleter GetDoc<CR>
"nnoremap <silent> <Leader>yf :YcmCompleter FixIt<CR>
"nnoremap <silent> <Leader>yg :YcmCompleter GoTo<CR>
"nnoremap <silent> <Leader>yi :YcmCompleter GoToInclude<CR>
"nnoremap <silent> <Leader>yt :YcmCompleter GetType<CR>

" Rebind easy motion
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

" Watch my .vimrc
"augroup reload_vimrc
"    autocmd!
"    autocmd BufWritePost $MYVIMRC source $MYVIMRC
"augroup end


augroup vimrc_autocmd
    autocmd!
    " Setup formatexpr specified filetype(s).
    autocmd FileType typescript,json,rust setl formatexpr=CocAction('formatSelected')
    " Update signature help on jump placeholder
    autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')

    " shitty taskwiki detection bullshit
    autocmd BufRead,BufNewFile /home/lain/pup/*/*.md set filetype=vimwiki

    autocmd FileType terraform set expandtab shiftwidth=2 softtabstop=2 tabstop=2
    autocmd BufRead,BufNewFile *.tf set expandtab shiftwidth=2 softtabstop=2 tabstop=2
    " Recognize NASM filetype
    autocmd BufRead,BufNewFile *.nasm set filetype=nasm


    " Remove trailing whitespace
    "autocmd BufWritePre * :%s/\s\+$//e

    autocmd FileType gitcommit setlocal spell
    autocmd FileType markdown setlocal spell tw=80
    autocmd FileType text setlocal spell
    autocmd FileType rst setlocal spell

    " Atomic save
    "autocmd BufWriteCmd * call AtomicSave()

    " graphviz/dot
    autocmd BufRead,BufNewFile *.gv set syntax=dot
    autocmd BufRead,BufNewFile *.dot set syntax=dot

    " msbuild
    autocmd BufRead,BufNewFile *.props set syntax=xml
    autocmd BufRead,BufNewFile *.targets set syntax=xml

    autocmd BufRead,BufNewFile *.js set sw=2 ts=2 expandtab
    autocmd BufRead,BufNewFile *.md set sw=2 ts=2 expandtab
    au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
    autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
    autocmd Filetype javascript set sw=2 ts=2 expandtab
    autocmd BufRead,BufNewFile * setlocal signcolumn=yes
    autocmd FileType tagbar,nerdtree setlocal signcolumn=no
augroup END
