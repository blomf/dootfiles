#!/usr/bin/zsh
comma=","
echo "File string: "
read search_string
echo "Path to grep (.): "
read working_path
echo "Filetypes (c/h/S/mk): "
read types

if [[ $working_path = "" ]]; then
        working_path="."
fi

if [[ $types = "" ]]; then
        types="c,h,S,mk"
fi

if [[ "$types" =~ "$comma" ]]; then
        echo "working"
        grep --include="\*.{"${types}"}" -rnw ${working_path} -e ${search_string}
else
        echo $types
        grep --include="\*""$types" -rnw ${working_path} -e ${search_string}
fi

