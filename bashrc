#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PATH=$PATH:/usr/lib/ccache/bin
export PATH

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
export http_proxy=''
export https_proxy=''
export ftp_proxy=''
export socks_proxy=''
