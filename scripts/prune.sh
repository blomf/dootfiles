#!/bin/bash


for f in *;
do
    cli old/$f;
    read -n 1 -p "Remove finding $f? " meme;
    if [ $meme == "y" ]; then
        rm $f;
    else
        cp $f valid;
    fi
    tput reset;
done
