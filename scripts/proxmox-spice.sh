#!/bin/bash

#
# SETTINGS
#
# Fill the following section with your prefered settings.
# Leaving the password field empty is much more secure. It will be prompted on the command line.
# Alternatively create a Proxmox user with limited privileges like shown in this template.
#

NODE="navi"
VMID="300"
PROXY=""
USERNAME=""
PASSWORD=""

#
# INITIALIZATION
#

if ! type "jq" > /dev/null; then
    echo 'Command line tool "jq" is needed. Please install.'
fi

if [ -z "$PASSWORD" ]; then
    read -s -p "Password: " PASSWORD
    echo
fi

if [ -z "$USERNAME" ]; then
    USERNAME=root@pam
fi

if [ -z "$PROXY" ]; then
    PROXY=$NODE
fi

#
# AUTHENTICATION PROCESS
#

RESPONSE=$(curl -f -s -S -k -d "username=$USERNAME&password=$PASSWORD"  "https://$PROXY:8006/api2/json/access/ticket")

if [ $? -ne 0 ]; then
    echo "ERROR: Authentication failed"
    exit 1
fi

TICKET=$(echo $RESPONSE | jq -r '.data.ticket')
CSRF=$(echo $RESPONSE | jq -r '.data.CSRFPreventionToken')

if [ -z "$TICKET" ] || [ -z "$CSRF" ]; then
    echo "ERROR: Could not process Authentication Ticket"
    exit 1
fi

#
# GET VM STATUS
#

RESPONSE=$(curl -f -s -S -k -b "PVEAuthCookie=$TICKET" -H "CSRFPreventionToken: $CSRF" "https://$PROXY:8006/api2/json/nodes/$NODE/qemu/$VMID/status/current")

STATUS=$(echo $RESPONSE | jq -r '.data.qmpstatus')

if [ $STATUS = "stopped" ]; then
    echo "ERROR: VM not running. Trying to start"
    RESPONSE=$(curl -d "" -f -s -S -k -b "PVEAuthCookie=$TICKET" -H "CSRFPreventionToken: $CSRF" "https://$PROXY:8006/api2/json/nodes/$NODE/qemu/$VMID/status/start")

    echo "Waiting 10 seconds before trying Spice connection ..."
    sleep 10
fi

#
# GET SPICE CONFIGURATION
#

RESPONSE=$(curl -f -s -S -k -b "PVEAuthCookie=$TICKET" -H "CSRFPreventionToken: $CSRF" "https://$PROXY:8006/api2/json/nodes/$NODE/qemu/$VMID/spiceproxy" -d "proxy=$PROXY")

if [ $? -ne 0 ]; then
    echo "ERROR: Maybe Proxmox-API changed?"
    exit 1
fi

#
# PARSING JSON RESPONSE
#

ATTENTION=$(echo $RESPONSE | jq -r '.data."secure-attention"')
DELETE=$(echo $RESPONSE | jq -r '.data."delete-this-file"')
PROXY=$(echo $RESPONSE | jq -r '.data.proxy')
TYPE=$(echo $RESPONSE | jq -r '.data.type')
CA=$(echo $RESPONSE | jq -r '.data.ca')
FULLSCREEN=$(echo $RESPONSE | jq -r '.data."toggle-fullscreen"')
TITLE=$(echo $RESPONSE | jq -r '.data.title')
HOST=$(echo $RESPONSE | jq -r '.data.host')
PASSWORD=$(echo $RESPONSE | jq -r '.data.password')
SUBJECT=$(echo $RESPONSE | jq -r '.data."host-subject"')
CURSOR=$(echo $RESPONSE | jq -r '.data."release-cursor"')
PORT=$(echo $RESPONSE | jq -r '.data."tls-port"')

#
# GENERATING REMOTE-VIEWER CONNECTION FILE
#

TMP=$(mktemp)

echo "[virt-viewer]" > $TMP
echo "secure-attention=${ATTENTION}" >> $TMP
echo "delete-this-file=${DELETE}" >> $TMP
echo "proxy=${PROXY}" >> $TMP
echo "type=${TYPE}" >> $TMP
echo "ca=${CA}" >> $TMP
echo "toggle-fullscreen=${FULLSCREEN}" >> $TMP
echo "title=${TITLE}" >> $TMP
echo "host=${HOST}" >> $TMP
echo "password=${PASSWORD}" >> $TMP
echo "host-subject=${SUBJECT}" >> $TMP
echo "release-cursor=${CURSOR}" >> $TMP
echo "tls-port=${PORT}" >> $TMP

#
# STARTING REMOTE-VIEWER
#

remote-viewer $TMP &
