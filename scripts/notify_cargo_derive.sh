##/bin/bash

# Requires inotifywait, clippy crate, cargo-modules crate, graphviz,
# easy-graph perl module, & dot_find_cycles.py
#
# Find Cycles: https://github.com/jantman/misc-scripts/blob/master/dot_find_cycles.py

# Usage: ./notify_cargo.sh <src dir to watch>

echo "Creating write watch on path $1"
while inotifywait --exclude "^.$" -r $1 -e close_write &> /dev/null; do

    rustup run nightly cargo modules graph > ./target/dependencies.dot
    /usr/bin/vendor_perl/graph-easy --output=./target/dependencies.txt ./target/dependencies.dot &> /dev/null &
    dot ./target/dependencies.dot -Tpng -o ./target/dependencies.png &> /dev/null &
    tput reset
    cargo build --release &> /dev/null
    rustup run nightly cargo clippy -- -Z unstable-options --pretty expanded

    if [ -z "$CYCLES" ]
    then
        printf '\n%s%s%s%s\n' "$(tput dim)" "$(tput setaf 6)" "＼(*^▽^*)/ No cycles detected! (づ｡◕‿‿◕｡)づ" "$(tput sgr0)"
    else
        CYCLES=$(python ~/dot_find_cycles.py ./target/dependencies.dot)
        printf '\n%s%s%s%s\n' "$(tput setaf 1)" "$(tput blink)" "╰[✿•̀o•́✿]╯ !!DEPENDENCY CYCLES DETECTED!! (╯°□°）╯  ┻━┻" "$(tput sgr0)"
        printf '%s%s%s\n' "$(tput setaf 3)" "$CYCLES" "$(tput sgr0)"
    fi

    r=$(((RANDOM%256)+1))
    printf "\n $(tput setaf $r)$(fortune kernelnewbies linux)$(tput sgr0) \n"
    touch ~/codez/src/ARC/culina/src/engine/state.rs
done
