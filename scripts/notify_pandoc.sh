echo "Creating write watch on path $@"
while inotifywait --exclude "^.$" -r $@ -e close_write &> /dev/null; do
    tput reset


pandoc -s -f markdown+smart -t revealjs --template=templates/default.revealjs ./day_4/slides.md -o slides.html -V revealjs-url=https://revealjs.com --slide-level 2 --css="./libs/custom.css" -V class=stretch -V theme=blood

    r=$(((RANDOM%256)+1))
    printf "\n $(tput setaf $r)$(fortune g linux)$(tput sgr0) \n"
done
