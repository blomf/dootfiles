##/bin/bash

# Requires inotifywait, clippy crate, cargo-modules crate, graphviz,
# easy-graph perl module, & dot_find_cycles.py
#
# Find Cycles: https://github.com/jantman/misc-scripts/blob/master/dot_find_cycles.py

# Usage: ./notify_cargo.sh <src dir to watch>

echo "Creating write watch on path $@"
while inotifywait --exclude "^.$" -r $@ -e close_write &> /dev/null; do

#    rustup run nightly cargo modules graph > ../target/dependencies.dot
#    /usr/bin/vendor_perl/graph-easy --output=../target/dependencies.txt ../target/dependencies.dot &> /dev/null &
#    dot ../target/dependencies.dot -Tpng -o ../target/dependencies.png &> /dev/null &
    tput reset
#    cargo doc --document-private-items
#    cargo +beta build &> /dev/null
#    cargo +beta test -- --nocapture
#    cargo +beta test --all-features
#    cargo +nightly build --all
#    cargo  run
#    cargo build --benches
#    cargo +nightly-2019-05-08 run
#    cargo +stable build -p engine
    cargo +stable build
#    cargo +stable run
#    cargo +stable test --no-run
#    cargo +stable clippy
#    cargo +nightly check
#    cargo +nightly build -p fuzz
#    cargo +nightly build -p engine
#    cargo +nightly xbuild
#    cargo +nightly-2019-09-05 clippy --all
#    cargo run
#    rustup run nightly cargo run -- -Z external-macro-backtrace

    if [ -z "$CYCLES" ]
    then
        printf '\n%s%s%s%s\n' "$(tput dim)" "$(tput setaf 6)" "＼(*^▽^*)/ No cycles detected! (づ｡◕‿‿◕｡)づ" "$(tput sgr0)"
    else
        CYCLES=$(python ~/dot_find_cycles.py ./target/dependencies.dot)
        printf '\n%s%s%s%s\n' "$(tput setaf 1)" "$(tput blink)" "╰[✿•̀o•́✿]╯ !!DEPENDENCY CYCLES DETECTED!! (╯°□°）╯  ┻━┻" "$(tput sgr0)"
        printf '%s%s%s\n' "$(tput setaf 3)" "$CYCLES" "$(tput sgr0)"
    fi

#    printf '\n%s\n' "$(cloc . | grep 'Language\|Rust')"

    r=$(((RANDOM%256)+1))
    printf "\n $(tput setaf $r)$(fortune g linux hackers)$(tput sgr0) \n"
done
