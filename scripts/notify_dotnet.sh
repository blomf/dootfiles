#/bin/bash

echo "Creating write watch on path $1"

echo "Creating write watch on path $@"
while inotifywait --exclude "^.$" -r $@ -e close_write &> /dev/null; do
    tput clear
    dotnet build
#    dotnet run
    r=$(((RANDOM%256)+1))
    printf "\n $(tput setaf $r)$(fortune g linux)$(tput sgr0) \n"
done
