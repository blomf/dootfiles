#!/bin/bash

x86_64-w64-mingw32-gcc -M -I /usr/x86_64-w64-mingw32/include win.cpp | sed -e 's/[\\ ]/\n/g' | \\
        sed -e '/^$/d' -e '/\.o:[ \t]*$/d' | \\
                ctags -L - --c++-kinds=fdctp --fields=ilamS --extra=+q

