#/bin/bash
echo "Creating write watch on path $1"
while inotifywait --exclude "^.$" -r $1 -e close_write &> /dev/null; do

    tput reset
    terraform apply --auto-approve
    dot ../output/projects.dot -Tpng -o ../output/projects.png &> /dev/null &

    printf '\n%s%s%s%s\n' "$(tput dim)" "$(tput setaf 6)" "＼(*^▽^*)/ (づ｡◕‿‿◕｡)づ" "$(tput sgr0)"

    r=$(((RANDOM%256)+1))
    printf "\n $(tput setaf $r)$(fortune linux hackers g)$(tput sgr0) \n"
done
