#/bin/bash

echo "Creating write watch on path $1"

inotifywait -m --exclude "^.$" -r $1 -e close_write |
    while read path action file; do
        tput reset
        cmake --build . -- -j8
        r=$(((RANDOM%256)+1))
        printf "\n $(tput setaf $r)$(fortune kernelnewbies linux) \n"
#        echo "The file '$file' appeared in directory '$path' via '$action'"
    done
